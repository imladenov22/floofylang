%token <int> INT
%token PLUS
%token MINUS
%token TIMES
%token DIV
%token EOL
%start main
%type <int> main
%%
main:
  expr EOL       { $1 }
;
expr:
  INT            { $1 }
| INT PLUS expr  { $1 + $3 }
| INT MINUS expr { $1 - $3 }
| INT TiMES expr { $1 * $3 }
| INT DIV expr   { $1 / $3 }
;