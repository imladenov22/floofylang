{
  open Fparser
  exception Eof
}
rule token = parse
  [' ' '\t' '\n']   { token lexbuf }
| ';'               { EOL }
| ['0'-'9']+ as lxm { INT(int_of_string lxm) }
| '+'               { PLUS }
| '-'               { MINUS }
| '*'               { TIMES }
| '/'               { DIV }
| eof               { raise Eof }